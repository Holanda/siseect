package datas;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import model.Projeto;
import util.Constantes;


public abstract class Padrao {

	public abstract String getTreino();
	public abstract String getTeste();
	public abstract String getValidacao();
	public abstract String getTreinoTeste();
	
	public abstract String getTreinoClassificadorEssembleKNN();
	public abstract String getTreinoClassificadorKNN_MLP_LR();
	public abstract String getTreinoClassificadorEssembleMLP();
	public abstract String getTreinoClassificadorKNN3_KNN5_KS_MLP_LR_AR_GP();
	public abstract String getTreinoClassificadorKNN3_MLP2_KS_MLP_LR_AR_GP();
	public abstract String getTreinoClassificadorKNN_MLP1_MLP2_LR_AR_GP();
	public abstract String getValidacaoClassificador();
	
	public abstract int getIndiceRotuloClassificador();

	public static List<Projeto> converteInstanciasParaProjetos(Padrao padrao,String nomeArquivo) throws IOException{

		// Usaremos a base de testes passada por par�metro.
		String caminhoArquivo = Constantes.CAMINHO_PADRAO
				+ nomeArquivo;
		
		return padrao.getProjetos(caminhoArquivo);
	}
	
	protected abstract List<Projeto> getProjetos(String caminhoArquivo) throws FileNotFoundException, IOException;
}
