package datas;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.Projeto;
import weka.core.Instances;


public class PadraoA3 extends Padrao{

	
	/**
	 * @param args
	 */
	
	private static final String ARQUIVO_TREINO = "A-3_TREINO.arff";
	private static final String ARQUIVO_TESTE = "A-3_TESTE.arff";
	private static final String ARQUIVO_VALIDACAO = "A-3_VALIDACAO.arff";
	private static final String ARQUIVO_TREINO_TESTE = "A-3_TREINO_TESTE.arff";

	
	private static final String ARQUIVO_TREINO_CLASSIFICADOR_ESSEMBLE_KNN = "A-3_TREINO_CLASSIFICADOR_ESSEMBLE_KNN.arff";
	private static final String ARQUIVO_TREINO_CLASSIFICADOR_ESSEMBLE_MLP = "A-3_TREINO_CLASSIFICADOR_ESSEMBLE_MLP.arff";
	private static final String ARQUIVO_TREINO_CLASSIFICADOR_KNN_MLP_LR = "A-3_TREINO_CLASSIFICADOR_ESSEMBLE_KNN_MLP_LR.arff";
	private static final String ARQUIVO_TREINO_CLASSIFICADOR_KNN3_KNN5_KS_MLP_LR_AR_GP = "A-3_TREINO_CLASSIFICADOR_ENSEMBLE_KNN3_KNN5_KS_MLP_LR_AR_GP.arff";
	private static final String ARQUIVO_TREINO_CLASSIFICADOR_KNN3_MLP2_KS_MLP_LR_AR_GP = "A-3_TREINO_CLASSIFICADOR_ENSEMBLE_KNN3_MLP2_KS_MLP_LR_AR_GP.arff";
	private static final String ARQUIVO_TREINO_CLASSIFICADOR_KNN_MLP1_MLP2_LR_AR_GP = "A-3_TREINO_CLASSIFICADOR_ENSEMBLE_KNN_MLP1_MLP2_LR_AR_GP.arff";
	private static final String ARQUIVO_VALIDACAO_CLASSIFICADOR = "A-3_VALIDACAO_CLASSIFICADOR.arff";
	
	private static final int INDICE_ROTULO = 5;

	@Override
	public List<Projeto> getProjetos(String caminhoArquivo)
			throws FileNotFoundException, IOException {
		
		List<Projeto> listaProjetos = new ArrayList<Projeto>();
		FileReader reader = new FileReader(caminhoArquivo);

		Instances instancias = new Instances(reader);

		int indice = 0;
		for (int contador = 0; contador < instancias.numInstances(); contador++, indice = 0) {

			Projeto projeto = new Projeto();
			projeto.setLinguagemProgramacaoPrimaria(instancias.instance(
					contador).stringValue(indice++));
			projeto.setPontosDeFuncoesClassificados(instancias.instance(contador).stringValue(indice++));
			projeto.setPlataformaDeDesenvolvimento(instancias.instance(contador).stringValue(indice++));
			projeto.setTipoLinguagem(instancias.instance(contador).stringValue(
					indice++));
			projeto.setTipoDesenvolvimento(instancias.instance(contador)
					.stringValue(indice++));
			projeto.setProdutividade(instancias.instance(contador).value(indice++));
			
			listaProjetos.add(projeto);

		}
		
		// Testando se a lista de projetos foi populada corretamente
		
		  /*for (Projeto projeto : listaProjetos) {
		  
		  System.out.print(projeto.getLinguagemProgramacaoPrimaria() + "\t");
		  System.out.print(projeto.getPontosFuncoesAjustados() + "\t");
		  System.out.print(projeto.getPlataformaDeDesenvolvimento() + "\t");
		  System.out.print(projeto.getTipoLinguagem() + "\t");
		  System.out.print(projeto.getTipoDesenvolvimento() + "\t");
		  System.out.print(projeto.getEsforcoSumarizado() + "\t");
		  
		  System.out.println(); 
		 }*/
		 

		return listaProjetos;
	}


	@Override
	public String getTreino() {
		return ARQUIVO_TREINO;
	}


	@Override
	public String getTeste() {
		return ARQUIVO_TESTE;
	}

	@Override
	public String getValidacao() {
		return ARQUIVO_VALIDACAO;
	}
	
	public String getTreinoTeste(){
		return ARQUIVO_TREINO_TESTE;
	}


	@Override
	public String getTreinoClassificadorEssembleKNN() {
		return ARQUIVO_TREINO_CLASSIFICADOR_ESSEMBLE_KNN;
	}


	@Override
	public String getTreinoClassificadorEssembleMLP() {
		return ARQUIVO_TREINO_CLASSIFICADOR_ESSEMBLE_MLP;
	}

	@Override
	public String getTreinoClassificadorKNN_MLP_LR() {
		return ARQUIVO_TREINO_CLASSIFICADOR_KNN_MLP_LR;
	}

	@Override
	public String getValidacaoClassificador() {
		return ARQUIVO_VALIDACAO_CLASSIFICADOR;
	}

	@Override
	public String getTreinoClassificadorKNN3_KNN5_KS_MLP_LR_AR_GP() {
		return ARQUIVO_TREINO_CLASSIFICADOR_KNN3_KNN5_KS_MLP_LR_AR_GP;
	}

	@Override
	public String getTreinoClassificadorKNN3_MLP2_KS_MLP_LR_AR_GP() {
		return ARQUIVO_TREINO_CLASSIFICADOR_KNN3_MLP2_KS_MLP_LR_AR_GP;
	}
	
	@Override
	public String getTreinoClassificadorKNN_MLP1_MLP2_LR_AR_GP() {
		return ARQUIVO_TREINO_CLASSIFICADOR_KNN_MLP1_MLP2_LR_AR_GP;
	}

	@Override
	public String toString() {
		return "PadraoA3";
	}


	@Override
	public int getIndiceRotuloClassificador() {
		return INDICE_ROTULO;
	}






	
}
