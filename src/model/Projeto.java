package model;

public class Projeto {

	private String linguagemProgramacaoPrimaria;
	private double pontosFuncoesAjustados;
	private String grupoDeAplicacao;
	private String arquitetura;
	private String clienteServidor;
	private String desenvolvimentoWeb;
	private String plataformaDeDesenvolvimento;
	private String setorDaIndustria;
	private String tipoLinguagem;
	private String tempoDeInatividade;
	private String tipoDesenvolvimento;
	private String mudancaDePessoal;
	private String usouDBMS;
	private double esforcoSumarizado;
	private double produtividade;
	private String pontosDeFuncoesClassificados;
	
	private double menorErro;
	private String melhorAlgoritmo;
	public String getLinguagemProgramacaoPrimaria() {
		return linguagemProgramacaoPrimaria;
	}
	public void setLinguagemProgramacaoPrimaria(String linguagemProgramacaoPrimaria) {
		this.linguagemProgramacaoPrimaria = linguagemProgramacaoPrimaria;
	}
	public double getPontosFuncoesAjustados() {
		return pontosFuncoesAjustados;
	}
	public void setPontosFuncoesAjustados(double pontosFuncoesAjustados) {
		this.pontosFuncoesAjustados = pontosFuncoesAjustados;
	}
	public String getGrupoDeAplicacao() {
		return grupoDeAplicacao;
	}
	public void setGrupoDeAplicacao(String grupoDeAplicacao) {
		this.grupoDeAplicacao = grupoDeAplicacao;
	}
	public String getArquitetura() {
		return arquitetura;
	}
	public void setArquitetura(String arquitetura) {
		this.arquitetura = arquitetura;
	}
	public String getClienteServidor() {
		return clienteServidor;
	}
	public void setClienteServidor(String clienteServidor) {
		this.clienteServidor = clienteServidor;
	}
	public String getDesenvolvimentoWeb() {
		return desenvolvimentoWeb;
	}
	public void setDesenvolvimentoWeb(String desenvolvimentoWeb) {
		this.desenvolvimentoWeb = desenvolvimentoWeb;
	}
	public String getPlataformaDeDesenvolvimento() {
		return plataformaDeDesenvolvimento;
	}
	public void setPlataformaDeDesenvolvimento(String plataformaDeDesenvolvimento) {
		this.plataformaDeDesenvolvimento = plataformaDeDesenvolvimento;
	}
	public String getSetorDaIndustria() {
		return setorDaIndustria;
	}
	public void setSetorDaIndustria(String setorDaIndustria) {
		this.setorDaIndustria = setorDaIndustria;
	}
	public String getTipoLinguagem() {
		return tipoLinguagem;
	}
	public void setTipoLinguagem(String tipoLinguagem) {
		this.tipoLinguagem = tipoLinguagem;
	}
	public String getTempoDeInatividade() {
		return tempoDeInatividade;
	}
	public void setTempoDeInatividade(String tempoDeInatividade) {
		this.tempoDeInatividade = tempoDeInatividade;
	}
	public String getTipoDesenvolvimento() {
		return tipoDesenvolvimento;
	}
	public void setTipoDesenvolvimento(String tipoDesenvolvimento) {
		this.tipoDesenvolvimento = tipoDesenvolvimento;
	}
	public String getMudancaDePessoal() {
		return mudancaDePessoal;
	}
	public void setMudancaDePessoal(String mudancaDePessoal) {
		this.mudancaDePessoal = mudancaDePessoal;
	}
	public String getUsouDBMS() {
		return usouDBMS;
	}
	public void setUsouDBMS(String usouDBMS) {
		this.usouDBMS = usouDBMS;
	}
	public double getEsforcoSumarizado() {
		return esforcoSumarizado;
	}
	public void setEsforcoSumarizado(double esforcoSumarizado) {
		this.esforcoSumarizado = esforcoSumarizado;
	}
	public double getMenorErro() {
		return menorErro;
	}
	public void setMenorErro(double menorErro) {
		this.menorErro = menorErro;
	}
	public String getMelhorAlgoritmo() {
		return melhorAlgoritmo;
	}
	public void setMelhorAlgoritmo(String melhorAlgoritmo) {
		this.melhorAlgoritmo = melhorAlgoritmo;
	}
	public double getProdutividade() {
		return produtividade;
	}
	public void setProdutividade(double produtividade) {
		this.produtividade = produtividade;
	}
	public String getPontosDeFuncoesClassificados() {
		return pontosDeFuncoesClassificados;
	}
	public void setPontosDeFuncoesClassificados(String pontosDeFuncoesClassificados) {
		this.pontosDeFuncoesClassificados = pontosDeFuncoesClassificados;
	}
}
