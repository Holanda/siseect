package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import util.Constantes;
import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

public class Resultado {

	private String nomeModelo;
	String relatorio = "";
	List<Double> mres = new ArrayList<Double>();
	List<Double> errosAbsolutos = new ArrayList<Double>();
	private double MMRE;
	private double MdMRE;
	private double pred25;
	private double erroAbsoluto;
	private double taxaDeAcertoDoClassificador;
	private boolean acertouAlgoritmo;
	private int acertosClassificador;
	private int errosClassificador;
	private double mmreIdeal;
	private double mdmreIdeal;
	private double pred25Ideal;
	private List<Double> mmresIdeais = new ArrayList<Double>();
	private List<Double> mdmresIdeais = new ArrayList<Double>();
	private List<Double> pred25Ideais = new ArrayList<Double>();
	
	
	public Resultado() {
		super();
	}

	public Resultado(String nomeModelo, ArrayList<String> atributosSelecionados) {
		super();
		this.nomeModelo = nomeModelo;
	}

	public List<Double> getMres() {
		return mres;
	}


	public String getNomeModelo() {
		return nomeModelo;
	}

	public void setNomeModelo(String nomeModelo) {
		this.nomeModelo = nomeModelo;
	}

	public List<Double> getErrosAbsolutos() {
		return errosAbsolutos;
	}

	public String getRelatorio() {
		return relatorio;
	}	
	
	public double getMMRE() {
		return MMRE;
	}

	public void calculaMMRE() {
		double somaMRE = 0;

		for (double erro : mres) {
			somaMRE += erro;
			if(Constantes.IMPRIMIR_ERRO_EXEMPLO){
				System.out.println(erro);
			}
		}

		this.MMRE = somaMRE / mres.size();

	}

	public double getMdMRE() {
		return MdMRE;
	}

	public void calculaMdMRE(){
		
		Collections.sort(mres);
		int esq=0;  
		int dir= mres.size()-1;  
		int meio;  
		meio=(esq+dir)/2;  
		this.MdMRE = mres.get(meio);  
	}
	
	public double getPred25() {
		return pred25;
	}
	
	public void calculaPred25(){
		
		int acertos = 0;
		
		for(double erro : mres){
			if (erro <= 25.99){
				acertos++;
			}
		}
		int tamanhoDaLista = mres.size();
		this.pred25 = (acertos*100)/tamanhoDaLista;
	}	

	public double getErroAbsoluto() {
		return erroAbsoluto;
	}

	public void calculaErroAbsoluto() {
		double somaErroAbsoluto = 0;

		for (Double erro : errosAbsolutos) {
			somaErroAbsoluto += erro;
		}

		this.erroAbsoluto = somaErroAbsoluto / errosAbsolutos.size();

	}
	
	public double getTaxaDeAcertoDoClassificador() {
		return taxaDeAcertoDoClassificador;
	}
	
	public void calculaTaxaDeAcertoDoClassificador(){
		taxaDeAcertoDoClassificador = acertosClassificador*100 / (acertosClassificador + errosClassificador);
		
	}

	public boolean isAcertouAlgoritmo() {
		return acertouAlgoritmo;
	}

	public void setAcertouAlgoritmo(boolean acertouAlgoritmo) {
		this.acertouAlgoritmo = acertouAlgoritmo;
	}
	
	public void addAcerto() {
		this.acertosClassificador++;
	}

	public void addErro(){
		this.errosClassificador++;
	}

	public double getMmreIdeal() {
		return mmreIdeal;
	}

	public List<Double> getMmresIdeais() {
		return mmresIdeais;
	}

	public void calculaMmreIdeal(){
		double somaMresIdeais = 0;

		for (Double erro : mmresIdeais) {
			somaMresIdeais += erro;
		}

		this.mmreIdeal = somaMresIdeais / mmresIdeais.size();

	}
	
	public double getMdmreIdeal() {
		return mdmreIdeal;
	}

	public List<Double> getMdmresIdeias() {
		return mdmresIdeais;
	}

	public void calculaMdmreIdeal(){
		Collections.sort(mmresIdeais);
		int esq=0;  
		int dir= mmresIdeais.size()-1;  
		int meio;  
		meio=(esq+dir)/2;  
		this.mdmreIdeal = mmresIdeais.get(meio);  

	}

	public double getPred25Ideal() {
		return pred25Ideal;
	}

	public List<Double> getPred25Ideais() {
		return pred25Ideais;
	}
	public void calculaPred25Ideal(){
		
		int acertos = 0;
		
		for(double erro : mmresIdeais){
			if (erro <= 25.99){
				acertos++;
			}
		}
		int tamanhoDaLista = mmresIdeais.size();
		this.pred25Ideal = (acertos*100)/tamanhoDaLista;
	}	

	public void avaliarModelo(Classifier classificador, Instances instancias, String nomeModeloCriado) throws Exception {
		
		// Agora vamos classificar cada dado original com esta rede
		double mre;
		double erroAbsoluto;
		Double valorEstimado, valorReal;
		this.nomeModelo = nomeModeloCriado;
		
		for (int a = 0; a < instancias.numInstances(); a++) {

			// Recuperamos cada uma das inst�ncias
			Instance instancia = instancias.instance(a);

			// Classificamos esta inst�ncia
			valorEstimado = (double) (classificador.classifyInstance(instancia));
			valorReal = (double) instancia.classValue();

			erroAbsoluto = Math
					.abs(valorReal - (valorEstimado));
			
			mre = erroAbsoluto / valorReal;

			errosAbsolutos.add(erroAbsoluto);
			mres.add(mre*100);

		}
		
		calculaMMRE();
		calculaMdMRE();
		calculaPred25();
		calculaErroAbsoluto();
		
		if(Constantes.IMPRIMIR_RESULTADO_INDIVIDUAL){
			System.out.println(toStringSimples());
		}
	}
	
	public String toStringSimples() {
	
		relatorio = "";

		relatorio += "Erro Absoluto M�dio: " + getErroAbsoluto() + "\n";
		relatorio += "MMRE: " + getMMRE() + "%" + "\n";
		relatorio += "MdMRE: " + getMdMRE() + "%" + "\n";
		relatorio += "Pred25: " + getPred25() + "%" + "\n";
		relatorio += "Taxa de Acerto do Classificador: " + getTaxaDeAcertoDoClassificador() + "%" + "\n";
		relatorio += "MMRE Ideal: " + getMmreIdeal() + "%" + "\n";
		relatorio += "MdMRE Ideal: " + getMdmreIdeal() + "%" + "\n";
		relatorio += "Pred25 Ideal: " + getPred25Ideal() + "%" + "\n";

		relatorio += nomeModelo;
		return relatorio;
	}
	@Override
	public String toString() {

	
		relatorio = "";

		for (Double erro : mres) {
			relatorio += "MRE: " + erro + "%\n";
		}

		relatorio += "Erro Absoluto M�dio: " + getErroAbsoluto() + "\n";
		relatorio += "MMRE: " + getMMRE() + "%" + "\n";
		relatorio += "MdMRE: " + getMdMRE() + "%" + "\n";
		relatorio += "Pred25: " + getPred25() + "%" + "\n";
		relatorio += "Taxa de Acerto do Classificador: " + getTaxaDeAcertoDoClassificador() + "%" + "\n";
		relatorio += "MMRE Ideal: " + getMmreIdeal() + "%" + "\n";
		relatorio += "MdMRE Ideal: " + getMdmreIdeal() + "%" + "\n";
		relatorio += "Pred25 Ideal: " + getPred25Ideal() + "%" + "\n";


		relatorio += nomeModelo;
		return relatorio;
	}

	
}
