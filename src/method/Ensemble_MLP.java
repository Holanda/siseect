package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import model.Resultado;
import model.ResultadoIdeal;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Attribute;
import weka.core.Instance;
import datas.Padrao;

public class Ensemble_MLP extends EnsembleTecnica {
	
	private static final String MLP1 = "MLP1";
	private static final String MLP2 = "MLP2";
	private static final String MLP3 = "MLP3";

	public Ensemble_MLP() {
	}
	
	
	private void avaliarMetodo(Padrao padrao, Resultado resultadoMLP1, Resultado resultadoMLP2,
			Resultado resultadoMLP3, Integer tipoClassificador ) throws Exception{
		
		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e os dados de classifica��o para o melhor algoritimo 
		calcularEstimativas(padrao, resultadoMLP1, resultadoMLP2, resultadoMLP3, tipoClassificador);
		
		// imprimi os resultados em tela
		imprimirResultados();
	}

	private void calcularEstimativas(Padrao padrao, Resultado resultadoMLP1,
			Resultado resultadoMLP2, Resultado resultadoMLP3, Integer tipoClassificador)
			throws Exception {

		
		double valorEstimado = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());
		
		//obtem o modelo knn1 criado
		ObjectInputStream oisMLP1 = new ObjectInputStream(new FileInputStream(resultadoMLP1.getNomeModelo()));
		MultilayerPerceptron mlp1= (MultilayerPerceptron) oisMLP1.readObject();
		oisMLP1.close();

		//obtem o modelo knn3 criado
		ObjectInputStream oisMLP2 = new ObjectInputStream(new FileInputStream(resultadoMLP2.getNomeModelo()));
		MultilayerPerceptron mlp2 = (MultilayerPerceptron) oisMLP2.readObject();
		oisMLP2.close();
		
		//obtem o modelo knn5 criado
		ObjectInputStream oisMLP3 = new ObjectInputStream(new FileInputStream(resultadoMLP3.getNomeModelo()));
		MultilayerPerceptron mlp3 = (MultilayerPerceptron) oisMLP3.readObject();
		oisMLP3.close();
		
		// configura as instancias com o classificador de melhor tecnica e sem o classificador 		
		configurarDadosClassificacao(padrao.getTreinoClassificadorEssembleMLP(), padrao.getValidacaoClassificador());
		
		// obtem o classificador treinado com as instancias que foram configuradas
		classificador = getClassificadorTreinado(tipoClassificador);

		// configura o r�tulo
		double rotuloClassificador = 0;
		Attribute rotulo = instanciasClassificadas.attribute(padrao.getIndiceRotuloClassificador());
		String algoritmoEscolhidoPeloClassificador;
		Instance instanciaSemClassificador;

		
		//calcular a dist�ncia de cada elemento da lista de projetos valida��o para a lista de projetos avaliados
		for(int i = 0 ; i < instancias.numInstances() ; i++){
			
			ResultadoIdeal resultadoIdeal;
			
			//pega cada instancia do conjunto de valida��o com rotulo de esfor�o
			instancia = instancias.instance(i);
			
			//pega cada instancia do conjunto de valida��o com rotulo de melhor algoritimo vazio
			instanciaSemClassificador = instanciasSemClassificacao.instance(i);
			
			//classifica a instancia com o melhor algoritimo usando algoritimo de classifica��o
			rotuloClassificador = classificador.classifyInstance(instanciaSemClassificador);
			
			//converte a classifica��o obtida para valor de caracteres que representa o melho algoritimo
			algoritmoEscolhidoPeloClassificador = rotulo.value((int)rotuloClassificador);

			//verifica o valor do classificador estimado
			if(algoritmoEscolhidoPeloClassificador.equals(MLP1) ){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor A
				valorEstimado = (double) (mlp1.classifyInstance(instancia));
				
			}else if(algoritmoEscolhidoPeloClassificador.equals(MLP2)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor B
				valorEstimado = (double) (mlp2.classifyInstance(instancia));

			}else if(algoritmoEscolhidoPeloClassificador.equals(MLP3)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor C
				valorEstimado = (double) (mlp3.classifyInstance(instancia));
				
			}else{
				throw new Exception("Melhor Algoritmo n�o encontrado");
			}

			
			// recupera o valor real do esfor�o
			valorReal = (double) instancia.classValue();
			
			// Processo para encontrar o melhor algoritimo na verdade e comparar com o escolhido
			resultadoIdeal = obtemResultadoIdeal(valorReal,
					instancia, mlp1,  mlp2, mlp3, algoritmoEscolhidoPeloClassificador);

			if (resultadoIdeal.isAcertouAlgoritmo()){
				resultado.addAcerto();
			}else{
				resultado.addErro();
			}
			
			
			//calcula o erro absoluto
			erroAbsoluto = Math
					.abs(valorReal - (valorEstimado));
			
			//calcula o mre
			mre = erroAbsoluto / valorReal;
			
			//adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);
			
			//adiciona o mre a lista de mres
			resultado.getMres().add(mre*100);
			
			//adiciona o mre ideal calculado no processo de encontrar o melhor algoritmo
			resultado.getMmresIdeais().add(resultadoIdeal.getMenorErroEstimado()*100);
		}
	}

	
	private ResultadoIdeal obtemResultadoIdeal(double valorReal,
			Instance instancia, MultilayerPerceptron mlp1, MultilayerPerceptron mlp2, MultilayerPerceptron mlp3,
			String algoritmoEscolhidoPeloClassificador) throws Exception {

		double valorEstimado, melhorValorEstimado;
		double erroAbs;
		double erroMre, menorErroMre;
		String melhorAlgoritmo;
		ResultadoIdeal resultadoIdeal = new ResultadoIdeal();

		
		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o knn
		valorEstimado = (double) (mlp1.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		
		melhorValorEstimado = valorEstimado;
		melhorAlgoritmo = MLP1;
		menorErroMre = erroMre;
		

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o mlp1
		valorEstimado = (double) (mlp2.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = MLP2;
			menorErroMre = erroMre;
		}

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o lr
		valorEstimado = (double) (mlp3.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = MLP3;
			menorErroMre = erroMre;
		}

		resultadoIdeal.setMelhorAlgoritmo(melhorAlgoritmo);
		resultadoIdeal.setMelhorValorEstimado(melhorValorEstimado);
		resultadoIdeal.setMenorErroEstimado(menorErroMre);
		
		if(melhorAlgoritmo.equals(algoritmoEscolhidoPeloClassificador)){
			resultadoIdeal.setAcertouAlgoritmo(true);
		}else{
			resultadoIdeal.setAcertouAlgoritmo(false);
		}
		
		return resultadoIdeal;
	}
	
	
	@Override
	public void run(Padrao padrao, Integer tipoClassificador) {

		try {
			
			//treina o mlp1
			Base_MLP mlp1 = new Base_MLP();
			mlp1.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 1);

			//treina o mlp2
			Base_MLP mlp2 = new Base_MLP();
			mlp2.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 2);

			//treina o mlp3
			Base_MLP mlp3 = new Base_MLP();
			mlp3.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 3);
			
			
			//carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao, padrao.getTreinoTeste()));
			
			//adiciona os campos melhor algoritimo e menor erro a lista de projetos
			adicionarCamposListaProjetosAvaliados(mlp1.getResultado(), mlp2.getResultado(),	mlp3.getResultado());
			
			//avalia o modelo combinado
			avaliarMetodo(padrao, mlp1.getResultado(), mlp2.getResultado(), mlp3.getResultado(), tipoClassificador);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public String toString() {
		return "ESSEMBLE_MLP";
	}

}
