package method;

import datas.Padrao;
import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.lazy.IBk;

public class Base_KNN extends Tecnica {

	
	public static final String KNN = "KNN";
	private IBk ibk;

	public Base_KNN() {

	}

	public void criarModelo(Integer k, String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);

		we = new WekaExperiment();

		we.setTrainingData(instancias);

		// o buildClassifier � feito aqui dentro.
		switch (k) {
		case 1:
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.KNN1); 
			break;
		case 3: 
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.KNN3);
			break;
		case 5: 
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.KNN5);
			break;
		case 7: 
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.KNN7);
			break;
		default:
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.KNN1); 
			break;
		}
		
		// retorna o classificador gerado
		ibk = (IBk) we.getClassifier();

		gerarModelo(ibk, KNN + k + "-" + nomeArquivoTreino);

	}

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		ibk = (IBk) recuperarModelo();
		configurarDados(nomeArquivoTeste);
		
		resultado = new Resultado();
		resultado.avaliarModelo(ibk, instancias, nomeModeloCriado);
	}

	
	
	public IBk getIbk() {
		return ibk;
	}

	public void setIbk(IBk ibk) {
		this.ibk = ibk;
	}

	public static String getKnn() {
		return KNN;
	}

	public void run(String treino, String validacao, Integer k) {
		try {
			
			criarModelo(k, treino);
			usarModelo(validacao);
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(Padrao padrao) {
		try {
			
			Base_KNN knn3 = new Base_KNN();
			knn3.criarModelo(3, padrao.getTreinoTeste());
			knn3.usarModelo(padrao.getValidacao());

			System.out.println("\n");
			Base_KNN knn5 = new Base_KNN();
			knn5.criarModelo(5,padrao.getTreinoTeste());
			knn5.usarModelo(padrao.getValidacao());
			
			System.out.println("\n");
			Base_KNN knn7 = new Base_KNN();
			knn7.criarModelo(7, padrao.getTreinoTeste());
			knn7.usarModelo(padrao.getValidacao());
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	

	
}
