package method;

import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.functions.GaussianProcesses;
import datas.Padrao;

public class Base_GP extends Tecnica {

	
	public static final String GP = "GP";
	private GaussianProcesses gp;

	public Base_GP() {

	}

	public void criarModelo(String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);

		we = new WekaExperiment();

		we.setTrainingData(instancias);

		
		
		we.KFoldCrossValidationStratified(10, 1, WekaExperiment.GP); 
		
		
		// retorna o classificador gerado
		gp = (GaussianProcesses) we.getClassifier();

		gerarModelo(gp, GP + "-" + nomeArquivoTreino);

		
	}

	

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		gp = (GaussianProcesses) recuperarModelo();
		configurarDados(nomeArquivoTeste);
		
		resultado = new Resultado();
		resultado.avaliarModelo(gp, instancias, nomeModeloCriado);
	}
	public void run(String treino, String validacao) {
		try {
			
			criarModelo(treino);
			usarModelo(validacao);
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(Padrao padrao) {
		
		try {

			Base_GP gp = new Base_GP();
			gp.criarModelo(padrao.getTreino());
			gp.usarModelo(padrao.getValidacao());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}


}
