package method;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.Constantes;
import util.WekaExperiment;
import weka.classifiers.Classifier;
import weka.core.Instances;
import model.Projeto;
import model.Resultado;
import datas.Padrao;

public abstract class EnsembleTecnica extends Tecnica {

	// inst�ncias da base classificada com o melhor algoritimo
	protected Instances instanciasClassificadas;

	// instancias da base sem a classifica��o de melhor algoritimo as quais
	// ser�o utilizadas pelo classificador
	protected Instances instanciasSemClassificacao;

	// classificador de melhor algoritimo
	protected Classifier classificador;
	
	List<Projeto> listaProjetos = new ArrayList<Projeto>();

	protected void imprimirResultados() throws Exception{
		
		resultado.calculaErroAbsoluto();
		resultado.calculaMMRE();
		resultado.calculaMdMRE();
		resultado.calculaPred25();
		resultado.calculaTaxaDeAcertoDoClassificador();
		resultado.calculaMmreIdeal();
		resultado.calculaMdmreIdeal();
		resultado.calculaPred25Ideal();
		System.out.println(resultado.toStringSimples());
	}
	protected void imprimirResultadosBasicos() throws Exception{
		
		resultado.calculaErroAbsoluto();
		resultado.calculaMMRE();
		resultado.calculaMdMRE();
		resultado.calculaPred25();
		System.out.println(resultado.toStringSimples());
	}	
	protected void adicionarCamposListaProjetosAvaliados(Resultado resultado1, Resultado resultado2,
			Resultado resultado3) {

		double menorErro;
		String melhorAlgoritmo;

		int tamanhoListaProjetos = this.listaProjetos.size();

		for (int i = 0; i < tamanhoListaProjetos; i++) {
			
			if (resultado1.getErrosAbsolutos().get(i) < resultado2.getErrosAbsolutos().get(i)) {

				menorErro = resultado1.getErrosAbsolutos().get(i);
				melhorAlgoritmo = resultado1.getNomeModelo();

			} else {

				menorErro = resultado2.getErrosAbsolutos().get(i);
				melhorAlgoritmo = resultado2.getNomeModelo();

			}
			if (resultado3.getErrosAbsolutos().get(i) < menorErro) {

				menorErro = resultado3.getErrosAbsolutos().get(i);
				melhorAlgoritmo = resultado3.getNomeModelo();

			}

			//projetos que vieram no par�metro recebe qual foi o menor erro obtido e o melhor algoritmo para o projeto
			listaProjetos.get(i).setMenorErro(menorErro);
			listaProjetos.get(i).setMelhorAlgoritmo(melhorAlgoritmo);

		}

		if(Constantes.IMPRIMIR_MELHOR_ALGORITMO){
		// impress�o dos valores dos dados do projeto
			for (Projeto projeto : listaProjetos) {
				
				//System.out.print(projeto.getLinguagemProgramacaoPrimaria()+ "\t");
				//System.out.print(projeto.getPontosFuncoesAjustados() + "\t");
				//System.out.print(projeto.getPlataformaDesenvolvimento() + "\t");
				//System.out.print(projeto.getTipoLinguagem() + "\t");
				//System.out.print(projeto.getTipoDesenvolvimento() + "\t");
				//System.out.print(projeto.getEsforcoSumarizado() + "\t");
	
				//System.out.print(projeto.getMenorErro() + "\t");
				System.out.println(projeto.getMelhorAlgoritmo());
			}
		}
	}
	
	protected Classifier getClassificadorTreinado(Integer tipoClassificador) throws Exception {
		//treino o classificador no arquivo com as instancias e o melhor m�todo encontrado
		we = new WekaExperiment();
		we.setTrainingData(instanciasClassificadas);
		we.KFoldCrossValidationStratified(10, 1, tipoClassificador); 
		
		// retorna o classificador gerado
		Classifier classificador = we.getClassifier();
		return classificador;
	}

	protected void configurarDadosClassificacao(String dadosTreino, String dadosValidacao)
			throws FileNotFoundException, IOException {
		
		// Usaremos a base definida no par�metro caminho, � o mesmo conjunto de dados mas agora com rotulo de classifica��o
		String nomeArquivoComClassificador = dadosTreino;
		caminhoArquivo = Constantes.CAMINHO_PADRAO + nomeArquivoComClassificador;
		reader = new FileReader(caminhoArquivo);
		this.instanciasClassificadas = new Instances(reader);
		this.instanciasClassificadas.setClassIndex(instanciasClassificadas.numAttributes() - 1);

		
		String nomeArquivoSemClassificador = dadosValidacao;
		caminhoArquivo = Constantes.CAMINHO_PADRAO + nomeArquivoSemClassificador;
		reader = new FileReader(caminhoArquivo);
		this.instanciasSemClassificacao = new Instances(reader);
		this.instanciasSemClassificacao.setClassIndex(instanciasSemClassificacao.numAttributes() - 1);
	}
	
	public List<Projeto> getListaProjetos() {
		return listaProjetos;
	}

	public void setListaProjetos(List<Projeto> listaProjetos) {
		this.listaProjetos = listaProjetos;
	}

	public abstract void run(Padrao padrao, Integer tipoClassificador);

	@Override
	public void run(Padrao padrao) {

	}

}
