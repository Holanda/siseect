package method;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Resultado;
import util.Constantes;
import util.WekaExperiment;
import weka.classifiers.Classifier;
import weka.core.Instances;
import datas.Padrao;

public abstract class Tecnica {

	protected String nomeModeloCriado;
	protected String caminhoArquivo;
	protected FileReader reader;
	protected Instances instancias;
	protected WekaExperiment we;
	protected Resultado resultado;
	
	protected void configurarDados(String nomeArquivo)
			throws Exception {

		// Usaremos a base definida no par�metro caminho
		caminhoArquivo = Constantes.CAMINHO_PADRAO + nomeArquivo;
		reader = new FileReader(caminhoArquivo);
		instancias = new Instances(reader);

		// Inicialmente os atributos ser�o fixos

		// configura o r�tulo
		instancias.setClassIndex(instancias.numAttributes() - 1);
		
	}

	protected void gerarModelo(Classifier classificador,
			String nomeArquivoTreino) throws IOException, FileNotFoundException {

		// serializando o modelo, por enquanto estamos criando o modelo, depois
		// essa parte deve ser desnecess�ria
		nomeModeloCriado = nomeArquivoTreino.replace("_TREINO", "") + ".ser";
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				nomeModeloCriado));
		oos.writeObject(classificador);
		oos.close();
	}

	protected Classifier recuperarModelo() throws IOException, FileNotFoundException,
			ClassNotFoundException {
		// Recuperamos o modelo criado pelo o KNN com k igual k
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				nomeModeloCriado));
		Classifier classificador = (Classifier) ois.readObject();
		ois.close();
		return classificador;
	}
	
	
	public String getNomeModeloCriado() {
		return nomeModeloCriado;
	}

	public void setNomeModeloCriado(String nomeModeloCriado) {
		this.nomeModeloCriado = nomeModeloCriado;
	}

	public String getCaminhoArquivo() {
		return caminhoArquivo;
	}

	public void setCaminhoArquivo(String caminhoArquivo) {
		this.caminhoArquivo = caminhoArquivo;
	}

	public FileReader getReader() {
		return reader;
	}

	public void setReader(FileReader reader) {
		this.reader = reader;
	}

	public Instances getInstancias() {
		return instancias;
	}

	public void setInstancias(Instances instancias) {
		this.instancias = instancias;
	}

	public WekaExperiment getWe() {
		return we;
	}

	public void setWe(WekaExperiment we) {
		this.we = we;
	}

	public Resultado getResultado() {
		return resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}
	

	public abstract void run(Padrao padrao);


	
	
	

}
