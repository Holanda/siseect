package method;

import datas.Padrao;
import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.functions.MultilayerPerceptron;

public class Base_MLP extends Tecnica {

	
	public static final String MLP = "MLP";
	MultilayerPerceptron mlp;

	public Base_MLP() {

	}

	public void criarModelo(Integer camadasOcultas, String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);
		
		//usando valida��o cruzada para criar o modelo	
		WekaExperiment we = new WekaExperiment();		

		// por enquanto coloco diretamente as inst�ncias para treino e teste, quando selecionar os atributos faremos a filtragem antes
		we.setTrainingData(instancias);
		
		// o buildClassifier � feito aqui dentro.
		switch (camadasOcultas) {
		case 1:
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.MLP1); 
			break;
		case 2: 
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.MLP2);
			break;
		case 3: 
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.MLP3);
			break;
		case 4: 
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.MLP4);
			break;
		default:
			we.KFoldCrossValidationStratified(10, 1, WekaExperiment.MLP1); 			
			break;
		}
		
		// retorna o classificador gerado
		mlp = (MultilayerPerceptron) we.getClassifier();

		gerarModelo(mlp, MLP + camadasOcultas + "-" + nomeArquivoTreino);

		
	}

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		
		mlp = (MultilayerPerceptron) recuperarModelo();
		configurarDados(nomeArquivoTeste);

		resultado = new Resultado();
		resultado.avaliarModelo(mlp, instancias, nomeModeloCriado);
	}

	public void run(String treino, String validacao, Integer camadasOcultas) {
		try {
			
			criarModelo(camadasOcultas, treino);
			usarModelo(validacao);
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(Padrao padrao) {
		
		try {
			
			Base_MLP mlp1 = new Base_MLP();
			mlp1.criarModelo(1, padrao.getTreinoTeste());
			mlp1.usarModelo(padrao.getValidacao());

			System.out.println("\n");

			Base_MLP mlp2 = new Base_MLP();
			mlp2.criarModelo(2, padrao.getTreinoTeste());
			mlp2.usarModelo(padrao.getValidacao());

			System.out.println("\n");

			Base_MLP mlp3 = new Base_MLP();
			mlp3.criarModelo(3, padrao.getTreinoTeste());
			mlp3.usarModelo(padrao.getValidacao());

			System.out.println("\n");

			Base_MLP mlp4 = new Base_MLP();
			mlp4.criarModelo(4, padrao.getTreinoTeste());
			mlp4.usarModelo(padrao.getValidacao());

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}		
	}
	
	
	
	


}
