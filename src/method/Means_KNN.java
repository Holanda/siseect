package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import model.Resultado;
import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import datas.Padrao;

public class Means_KNN extends MeansTecnica {

	public Means_KNN() {
	}

	private void avaliarMetodo(Padrao padrao,
			Resultado resultadoKNN3, Resultado resultadoKNN5, Resultado resultadoKNN7) throws Exception {

		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e
		// os dados de classifica��o para o melhor algoritimo
		calcularEstimativas(padrao, resultadoKNN3,
				resultadoKNN5, resultadoKNN7);

		// imprimi os resultados em tela
		imprimirResultadosBasicos();
	}

	private void calcularEstimativas(Padrao padrao,
			Resultado resultadoKNN3, Resultado resultadoKNN5, Resultado resultadoKNN7) throws Exception {

		double valorEstimado = 0;
		double valorEstimadoIBK3 = 0;
		double valorEstimadoIBK5 = 0;
		double valorEstimadoIBK7 = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());


		// obtem o modelo knn3 criado
		ObjectInputStream oisKNN3 = new ObjectInputStream(new FileInputStream(
				resultadoKNN3.getNomeModelo()));
		IBk ibk3 = (IBk) oisKNN3.readObject();
		oisKNN3.close();

		// obtem o modelo knn5 criado
		ObjectInputStream oisKNN5 = new ObjectInputStream(new FileInputStream(
				resultadoKNN5.getNomeModelo()));
		IBk ibk5 = (IBk) oisKNN5.readObject();
		oisKNN5.close();

		// obtem o modelo knn7 criado
		ObjectInputStream oisKNN7 = new ObjectInputStream(new FileInputStream(
				resultadoKNN7.getNomeModelo()));
		IBk ibk7 = (IBk) oisKNN7.readObject();
		oisKNN7.close();

		// calcular a dist�ncia de cada elemento da lista de projetos valida��o
		// para a lista de projetos avaliados
		for (int i = 0; i < instancias.numInstances(); i++) {

			// pega cada instancia do conjunto de valida��o com rotulo de
			// esfor�o
			instancia = instancias.instance(i);

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor KNN3
			valorEstimadoIBK3 = (double) (ibk3.classifyInstance(instancia));

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor KNN5
			valorEstimadoIBK5 = (double) (ibk5.classifyInstance(instancia));

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor KNN7
			valorEstimadoIBK7 = (double) (ibk7.classifyInstance(instancia));

			valorEstimado = (valorEstimadoIBK3 + valorEstimadoIBK5 + valorEstimadoIBK7)/3;

			// recupera o valor real do esfor�o
			valorReal = (double) instancia.classValue();

			// calcula o erro absoluto
			erroAbsoluto = Math.abs(valorReal - (valorEstimado));

			// calcula o mre
			mre = erroAbsoluto / valorReal;

			// adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);

			// adiciona o mre a lista de mres
			resultado.getMres().add(mre * 100);

		}
	}


	@Override
	public void run(Padrao padrao) {

		try {


			// treina o knn3
			Base_KNN knn3 = new Base_KNN();
			knn3.run(padrao.getTreinoTeste(), padrao.getValidacao(), 3);

			// treina o knn5
			Base_KNN knn5 = new Base_KNN();
			knn5.run(padrao.getTreinoTeste(), padrao.getValidacao(), 5);

			// treina o knn7
			Base_KNN knn7 = new Base_KNN();
			knn7.run(padrao.getTreinoTeste(), padrao.getValidacao(), 7);

			// carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao,
					padrao.getTeste()));

			// avalia o modelo combinado
			avaliarMetodo(padrao, knn3.getResultado(),
					knn5.getResultado(), knn7.getResultado());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "MEANS_KNN";
	}

}
