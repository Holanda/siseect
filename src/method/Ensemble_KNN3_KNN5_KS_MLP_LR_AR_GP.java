package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import model.Projeto;
import model.Resultado;
import model.ResultadoIdeal;
import util.Constantes;
import weka.classifiers.functions.GaussianProcesses;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.lazy.KStar;
import weka.classifiers.meta.AdditiveRegression;
import weka.core.Attribute;
import weka.core.Instance;
import datas.Padrao;

public class Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP extends EnsembleTecnica {
	
	private static final String KNN3 = "KNN3";
	private static final String KNN5 = "KNN5";
	private static final String KS = "KS";
	private static final String MLP = "MLP";
	private static final String LR = "LR";
	private static final String AR = "AR";
	private static final String GP = "GP";

	public Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP() {
	}
	
	private void avaliarMetodo(Padrao padrao, Resultado resultadoKNN3, Resultado resultadoKNN5, Resultado resultadoKStar, Resultado resultadoMLP,
			Resultado resultadoLinearRegression, Resultado resultadoAdditiveRegression, Resultado resultadoGaussianProcess, Integer tipoClassificador ) throws Exception{
		
		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e os dados de classifica��o para o melhor algoritimo 
		calcularEstimativas(padrao, resultadoKNN3, resultadoKNN5, resultadoKStar, resultadoMLP, resultadoLinearRegression, resultadoAdditiveRegression, resultadoGaussianProcess, tipoClassificador);
		
		// imprimi os resultados em tela
		imprimirResultados();
	}

	private void calcularEstimativas(Padrao padrao, Resultado resultadoKNN3, Resultado resultadoKNN5,
			Resultado resultadoKStar, Resultado resultadoMLP, Resultado resultadoLinearRegression, Resultado resultadoAdditiveRegression, Resultado resultadoGaussianProcess, Integer tipoClassificador)
			throws Exception {

		ResultadoIdeal resultadoIdeal;

		
		double valorEstimado = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());
		
		//obtem o modelo knn3 criado
		ObjectInputStream oisKNN3 = new ObjectInputStream(new FileInputStream(resultadoKNN3.getNomeModelo()));
		IBk knn3 = (IBk) oisKNN3.readObject();
		oisKNN3.close();

		//obtem o modelo knn5 criado
		ObjectInputStream oisKNN5 = new ObjectInputStream(new FileInputStream(resultadoKNN5.getNomeModelo()));
		IBk knn5 = (IBk) oisKNN5.readObject();
		oisKNN5.close();

		//obtem o modelo kstar criado
		ObjectInputStream oisKS = new ObjectInputStream(new FileInputStream(resultadoKStar.getNomeModelo()));
		KStar kstar = (KStar) oisKS.readObject();
		oisKS.close();

		//obtem o modelo mlp criado
		ObjectInputStream oisMLP = new ObjectInputStream(new FileInputStream(resultadoMLP.getNomeModelo()));
		MultilayerPerceptron mlp = (MultilayerPerceptron) oisMLP.readObject();
		oisMLP.close();
		
		//obtem o modelo lr criado
		ObjectInputStream oisLR = new ObjectInputStream(new FileInputStream(resultadoLinearRegression.getNomeModelo()));
		LinearRegression lr = (LinearRegression) oisLR.readObject();
		oisLR.close();

		//obtem o modelo ar criado
		ObjectInputStream oisAR = new ObjectInputStream(new FileInputStream(resultadoAdditiveRegression.getNomeModelo()));
		AdditiveRegression ar = (AdditiveRegression) oisAR.readObject();
		oisAR.close();

		//obtem o modelo gp criado
		ObjectInputStream oisGP = new ObjectInputStream(new FileInputStream(resultadoGaussianProcess.getNomeModelo()));
		GaussianProcesses gp = (GaussianProcesses) oisGP.readObject();
		oisGP.close();

		// configura as instancias com o classificador de melhor tecnica e sem o classificador 		
		configurarDadosClassificacao(padrao.getTreinoClassificadorKNN3_KNN5_KS_MLP_LR_AR_GP(), padrao.getValidacaoClassificador());
		
		// obtem o classificador treinado com as instancias que foram configuradas
		classificador = getClassificadorTreinado(tipoClassificador);

		// configura o r�tulo
		double rotuloClassificador = 0;
		Attribute rotulo = instanciasClassificadas.attribute(padrao.getIndiceRotuloClassificador());
		String algoritmoEscolhidoPeloClassificador;
		Instance instanciaSemClassificador;

		
		//calcular a dist�ncia de cada elemento da lista de projetos valida��o para a lista de projetos avaliados
		for(int i = 0 ; i < instancias.numInstances() ; i++){
			
			//pega cada instancia do conjunto de valida��o com rotulo de esfor�o
			instancia = instancias.instance(i);
			
			//pega cada instancia do conjunto de valida��o com rotulo de melhor algoritimo vazio
			instanciaSemClassificador = instanciasSemClassificacao.instance(i);
			
			//classifica a instancia com o melhor algoritimo usando algoritimo de classifica��o
			rotuloClassificador = classificador.classifyInstance(instanciaSemClassificador);
			
			//converte a classifica��o obtida para valor de caracteres que representa o melho algoritimo
			algoritmoEscolhidoPeloClassificador = rotulo.value((int)rotuloClassificador);

			//verifica o valor do classificador estimado
			if(algoritmoEscolhidoPeloClassificador.equals(KNN3) ){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor knn3
				valorEstimado = (double) (knn3.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();
				
			}else if(algoritmoEscolhidoPeloClassificador.equals(KNN5)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor knn5
				valorEstimado = (double) (knn5.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();

			}else if(algoritmoEscolhidoPeloClassificador.equals(KS)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor mlp
				valorEstimado = (double) (kstar.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();

			}else if(algoritmoEscolhidoPeloClassificador.equals(MLP)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor mlp
				valorEstimado = (double) (mlp.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();

			}else if(algoritmoEscolhidoPeloClassificador.equals(LR)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor lr
				valorEstimado = (double) (lr.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();
				
			}else if(algoritmoEscolhidoPeloClassificador.equals(AR)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor mlp
				valorEstimado = (double) (ar.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();

			}else if(algoritmoEscolhidoPeloClassificador.equals(GP)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor mlp
				valorEstimado = (double) (gp.classifyInstance(instancia));
				valorReal = (double) instancia.classValue();

			}else{
				throw new Exception("Melhor Algoritmo n�o encontrado");
			}
			
			
			// Processo para encontrar o melhor algoritimo na verdade e comparar com o escolhido
			
			resultadoIdeal = obtemResultadoIdeal(valorReal,
					instancia, knn3, knn5, kstar, mlp, lr, ar, gp, algoritmoEscolhidoPeloClassificador);

			if (resultadoIdeal.isAcertouAlgoritmo()){
				resultado.addAcerto();
			}else{
				resultado.addErro();
			}
						
						
			//calcula o erro absoluto
			erroAbsoluto = Math
					.abs(valorReal - (valorEstimado));
			
			//calcula o mre
			mre = erroAbsoluto / valorReal;
			
			//adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);
			
			//adiciona o mre a lista de mres
			resultado.getMres().add(mre*100);

			//adiciona o mre ideal calculado no processo de encontrar o melhor algoritmo
			resultado.getMmresIdeais().add(resultadoIdeal.getMenorErroEstimado()*100);
		}
	}
	
	private ResultadoIdeal obtemResultadoIdeal(double valorReal,
			Instance instancia, IBk knn3, IBk knn5, KStar kstar,
			MultilayerPerceptron mlp, LinearRegression lr,
			AdditiveRegression ar, GaussianProcesses gp, String algoritmoEscolhidoPeloClassificador) throws Exception {

		double valorEstimado, melhorValorEstimado;
		double erroAbs;
		double erroMre, menorErroMre;
		String melhorAlgoritmo;
		ResultadoIdeal resultadoIdeal = new ResultadoIdeal();

		
		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o knn
		valorEstimado = (double) (knn3.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		
		melhorValorEstimado = valorEstimado;
		melhorAlgoritmo = KNN3;
		menorErroMre = erroMre;
		
		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o mlp1
		valorEstimado = (double) (knn5.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = KNN5;
			menorErroMre = erroMre;
		}
		
		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o mlp2
		valorEstimado = (double) (kstar.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = KS;
			menorErroMre = erroMre;
		}
		
		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o mlp1
		valorEstimado = (double) (mlp.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = MLP;
			menorErroMre = erroMre;
		}

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o lr
		valorEstimado = (double) (lr.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = LR;
			menorErroMre = erroMre;
		}

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o ar
		valorEstimado = (double) (ar.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = AR;
			menorErroMre = erroMre;
		}


		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o gp
		valorEstimado = (double) (gp.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = GP;
			menorErroMre = erroMre;
		}
		
		resultadoIdeal.setMelhorAlgoritmo(melhorAlgoritmo);
		resultadoIdeal.setMelhorValorEstimado(melhorValorEstimado);
		resultadoIdeal.setMenorErroEstimado(menorErroMre);
		
		if(melhorAlgoritmo.equals(algoritmoEscolhidoPeloClassificador)){
			resultadoIdeal.setAcertouAlgoritmo(true);
		}else{
			resultadoIdeal.setAcertouAlgoritmo(false);
		}
		
		return resultadoIdeal;
	}
	
	@Override
	public void run(Padrao padrao, Integer tipoClassificador) {

		try {
			
			//treina o KNN3
			Base_KNN knn3 = new Base_KNN();
			knn3.run(padrao.getTreino(), padrao.getTeste(), 3);

			//treina o KNN5
			Base_KNN knn5 = new Base_KNN();
			knn5.run(padrao.getTreino(), padrao.getTeste(), 5);

			//treina o Kstar
			Base_KS kstar = new Base_KS();
			kstar.run(padrao.getTreino(), padrao.getTeste());

			//treina o MultiLayer Percepton
			Base_MLP mlp = new Base_MLP();
			mlp.run(padrao.getTreino(), padrao.getTeste(), 1);

			//treina o Linear Regression
			Base_LR regressaoLinear = new Base_LR();
			regressaoLinear.run(padrao.getTreino(), padrao.getTeste());

			//treina o Additive Regression
			Base_AR regressaoAditiva = new Base_AR();
			regressaoAditiva.run(padrao.getTreino(), padrao.getTeste());

			//treina o Gaussian Process
			Base_GP processoGaussiano = new Base_GP();
			processoGaussiano.run(padrao.getTreino(), padrao.getTeste());

			
			//carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao, padrao.getTeste()));
			
			//adiciona os campos melhor algoritimo e menor erro a lista de projetos
			adicionarCamposListaProjetosAvaliados(knn3.getResultado(),
					knn5.getResultado(), kstar.getResultado(),
					mlp.getResultado(), regressaoLinear.getResultado(),
					regressaoAditiva.getResultado(),
					processoGaussiano.getResultado());
			
			//avalia o modelo combinado
			avaliarMetodo(padrao, knn3.getResultado(), knn5.getResultado(), kstar.getResultado(), mlp.getResultado(), regressaoLinear.getResultado(), regressaoAditiva.getResultado(), processoGaussiano.getResultado(), tipoClassificador);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void adicionarCamposListaProjetosAvaliados(Resultado resultado1,
			Resultado resultado2, Resultado resultado3, Resultado resultado4,
			Resultado resultado5, Resultado resultado6, Resultado resultado7) {
		

		int tamanhoListaProjetos = this.listaProjetos.size();

		
		for (int i = 0; i < tamanhoListaProjetos; i++) {
			
			List<Resultado> resultados = new ArrayList<Resultado>();
			resultados.add(resultado1);
			resultados.add(resultado2);
			resultados.add(resultado3);
			resultados.add(resultado4);
			resultados.add(resultado5);
			resultados.add(resultado6);
			resultados.add(resultado7);
			
			Projeto projetoComMelhoresResultados = menorResultado(resultados, i);
			

			//projetos que vieram no par�metro recebe qual foi o menor erro obtido e o melhor algoritmo para o projeto
			listaProjetos.get(i).setMenorErro(projetoComMelhoresResultados.getMenorErro());
			listaProjetos.get(i).setMelhorAlgoritmo(projetoComMelhoresResultados.getMelhorAlgoritmo());

		}

		if(Constantes.IMPRIMIR_MELHOR_ALGORITMO){
		// impress�o dos valores dos dados do projeto
			for (Projeto projeto : listaProjetos) {
				
				//System.out.print(projeto.getLinguagemProgramacaoPrimaria()+ "\t");
				//System.out.print(projeto.getPontosFuncoesAjustados() + "\t");
				//System.out.print(projeto.getPlataformaDesenvolvimento() + "\t");
				//System.out.print(projeto.getTipoLinguagem() + "\t");
				//System.out.print(projeto.getTipoDesenvolvimento() + "\t");
				//System.out.print(projeto.getEsforcoSumarizado() + "\t");
	
				System.out.print(projeto.getMenorErro() + "\t");
				System.out.println(projeto.getMelhorAlgoritmo());
			}
		}
	}

	private Projeto menorResultado(List<Resultado> resultados, int i) {
		double menorErro = 10000000;
		String melhorAlgoritimo = "";
		Projeto projeto = new Projeto();
		
		// faz uma itera��o na lista dos resultados que foram passados
		for (Resultado resultado : resultados) {
			if(resultado.getMres().get(i) < menorErro){
				menorErro = resultado.getMres().get(i);
				melhorAlgoritimo = resultado.getNomeModelo();
			}
		}
		projeto.setMenorErro(menorErro);
		projeto.setMelhorAlgoritmo(melhorAlgoritimo);
		
		return projeto;
	}

	@Override
	public String toString() {
		return "ESSEMBLE_KNN3_KNN5_KSTAR_MLP_LR_AR_GP";
	}
}
