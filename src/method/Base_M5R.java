package method;

import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.rules.M5Rules;
import datas.Padrao;

public class Base_M5R extends Tecnica {

	
	public static final String M5R = "M5R";
	private M5Rules m5R;

	public Base_M5R() {

	}

	public void criarModelo(String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);

		we = new WekaExperiment();

		we.setTrainingData(instancias);

		
		
		we.KFoldCrossValidationStratified(10, 1, WekaExperiment.M5R); 
		
		
		// retorna o classificador gerado
		m5R = (M5Rules) we.getClassifier();

		gerarModelo(m5R, M5R + "-" + nomeArquivoTreino);

		
	}

	

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		m5R = (M5Rules) recuperarModelo();
		configurarDados(nomeArquivoTeste);
		
		resultado = new Resultado();
		resultado.avaliarModelo(m5R, instancias, nomeModeloCriado);
	}
	@Override
	public void run(Padrao padrao) {
		try {
			
			Base_M5R m5R = new Base_M5R();
			m5R.criarModelo(padrao.getTreino());
			m5R.usarModelo(padrao.getValidacao());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}

}
