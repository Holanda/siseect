package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import model.Resultado;
import model.ResultadoIdeal;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.Instance;
import datas.Padrao;

public class Ensemble_KNN extends EnsembleTecnica {

	private static final String KNN3 = "KNN3";
	private static final String KNN5 = "KNN5";
	private static final String KNN7 = "KNN7";

	public Ensemble_KNN() {
	}

	private void avaliarMetodo(Padrao padrao, Resultado resultadoKNN3,
			Resultado resultadoKNN5, Resultado resultadoKNN7,
			Integer tipoClassificador) throws Exception {

		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e
		// os dados de classifica��o para o melhor algoritimo
		calcularEstimativas(padrao, resultadoKNN3, resultadoKNN5,
				resultadoKNN7, tipoClassificador);

		// imprimi os resultados em tela
		imprimirResultados();
	}

	private void calcularEstimativas(Padrao padrao, Resultado resultadoKNN3,
			Resultado resultadoKNN5, Resultado resultadoKNN7,
			Integer tipoClassificador) throws Exception {

		double valorEstimado = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());

		// obtem o modelo knn3 criado
		ObjectInputStream oisKNN3 = new ObjectInputStream(new FileInputStream(
				resultadoKNN3.getNomeModelo()));
		IBk ibk3 = (IBk) oisKNN3.readObject();
		oisKNN3.close();

		// obtem o modelo knn5 criado
		ObjectInputStream oisKNN5 = new ObjectInputStream(new FileInputStream(
				resultadoKNN5.getNomeModelo()));
		IBk ibk5 = (IBk) oisKNN5.readObject();
		oisKNN5.close();

		// obtem o modelo knn7 criado
		ObjectInputStream oisKNN7 = new ObjectInputStream(new FileInputStream(
				resultadoKNN7.getNomeModelo()));
		IBk ibk7 = (IBk) oisKNN7.readObject();
		oisKNN7.close();

		// configura as instancias com o classificador de melhor tecnica e sem o
		// classificador
		configurarDadosClassificacao(
				padrao.getTreinoClassificadorEssembleKNN(),
				padrao.getValidacaoClassificador());

		// obtem o classificador treinado com as instancias que foram
		// configuradas
		classificador = getClassificadorTreinado(tipoClassificador);

		// configura o r�tulo
		double rotuloClassificador = 0;
		Attribute rotulo = instanciasClassificadas.attribute(padrao
				.getIndiceRotuloClassificador());
		String algoritmoEscolhidoPeloClassificador;
		Instance instanciaSemClassificador;

		// calcular a dist�ncia de cada elemento da lista de projetos valida��o
		// para a lista de projetos avaliados
		for (int i = 0; i < instancias.numInstances(); i++) {

			ResultadoIdeal resultadoIdeal;

			// pega cada instancia do conjunto de valida��o com rotulo de
			// esfor�o
			instancia = instancias.instance(i);

			// pega cada instancia do conjunto de valida��o com rotulo de melhor
			// algoritimo vazio
			instanciaSemClassificador = instanciasSemClassificacao.instance(i);

			// classifica a instancia com o melhor algoritimo usando algoritimo
			// de classifica��o
			rotuloClassificador = classificador
					.classifyInstance(instanciaSemClassificador);

			// converte a classifica��o obtida para valor de caracteres que
			// representa o melho algoritimo
			algoritmoEscolhidoPeloClassificador = rotulo
					.value((int) rotuloClassificador);

			// verifica o valor do classificador estimado
			if (algoritmoEscolhidoPeloClassificador.equals(KNN3)) {

				// Classificamos esta inst�ncia com o algoritimo relativo ao
				// valor B
				valorEstimado = (double) (ibk3.classifyInstance(instancia));

			} else if (algoritmoEscolhidoPeloClassificador.equals(KNN5)) {

				// Classificamos esta inst�ncia com o algoritimo relativo ao
				// valor C
				valorEstimado = (double) (ibk5.classifyInstance(instancia));

			} else if (algoritmoEscolhidoPeloClassificador.equals(KNN7)) {

				// Classificamos esta inst�ncia com o algoritimo relativo ao
				// valor A
				valorEstimado = (double) (ibk7.classifyInstance(instancia));

			} else {
				throw new Exception("Melhor Algoritmo n�o encontrado");
			}

			// recupera o valor real do esfor�o
			valorReal = (double) instancia.classValue();

			// Processo para encontrar o melhor algoritimo na verdade e comparar
			// com o escolhido

			resultadoIdeal = obtemResultadoIdeal(valorReal, instancia, ibk3,
					ibk5, ibk7, algoritmoEscolhidoPeloClassificador);

			if (resultadoIdeal.isAcertouAlgoritmo()) {
				resultado.addAcerto();
			} else {
				resultado.addErro();
			}

			// calcula o erro absoluto
			erroAbsoluto = Math.abs(valorReal - (valorEstimado));

			// calcula o mre
			mre = erroAbsoluto / valorReal;

			// adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);

			// adiciona o mre a lista de mres
			resultado.getMres().add(mre * 100);

			// adiciona o mre ideal calculado no processo de encontrar o melhor
			// algoritmo
			resultado.getMmresIdeais().add(
					resultadoIdeal.getMenorErroEstimado() * 100);
		}
	}

	private ResultadoIdeal obtemResultadoIdeal(double valorReal,
			Instance instancia, IBk knn3, IBk knn5, IBk knn7,
			String algoritmoEscolhidoPeloClassificador) throws Exception {

		double valorEstimado, melhorValorEstimado;
		double erroAbs;
		double erroMre, menorErroMre;
		String melhorAlgoritmo;
		ResultadoIdeal resultadoIdeal = new ResultadoIdeal();

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para
		// o knn3
		valorEstimado = (double) (knn3.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		melhorValorEstimado = valorEstimado;
		melhorAlgoritmo = KNN3;
		menorErroMre = erroMre;

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para
		// o knn5
		valorEstimado = (double) (knn5.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre) {
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = KNN5;
			menorErroMre = erroMre;
		}

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para
		// o knn
		valorEstimado = (double) (knn7.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre) {

			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = KNN7;
			menorErroMre = erroMre;
		}
		
		resultadoIdeal.setMelhorAlgoritmo(melhorAlgoritmo);
		resultadoIdeal.setMelhorValorEstimado(melhorValorEstimado);
		resultadoIdeal.setMenorErroEstimado(menorErroMre);

		if (melhorAlgoritmo.equals(algoritmoEscolhidoPeloClassificador)) {
			resultadoIdeal.setAcertouAlgoritmo(true);
		} else {
			resultadoIdeal.setAcertouAlgoritmo(false);
		}

		return resultadoIdeal;
	}

	@Override
	public void run(Padrao padrao, Integer tipoClassificador) {

		try {

			// treina o knn1
			// Base_KNN knn1 = new Base_KNN();
			// knn1.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 1);

			// treina o knn3
			Base_KNN knn3 = new Base_KNN();
			knn3.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 3);

			// treina o knn5
			Base_KNN knn5 = new Base_KNN();
			knn5.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 5);

			// treina o knn7
			Base_KNN knn7 = new Base_KNN();
			knn7.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 7);

			// carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao,
					padrao.getTreinoTeste()));

			// adiciona os campos melhor algoritimo e menor erro a lista de
			// projetos
			adicionarCamposListaProjetosAvaliados(knn3.getResultado(),
					knn5.getResultado(), knn7.getResultado());

			// avalia o modelo combinado
			avaliarMetodo(padrao, knn3.getResultado(), knn5.getResultado(), knn7.getResultado(), tipoClassificador);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "EssembleKNN";
	}

}
