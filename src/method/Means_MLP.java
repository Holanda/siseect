package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import model.Resultado;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instance;
import datas.Padrao;

public class Means_MLP extends MeansTecnica{
	
	public Means_MLP() {
	}
	
	
	private void avaliarMetodo(Padrao padrao, Resultado resultadoMLP1, Resultado resultadoMLP2,
			Resultado resultadoMLP3) throws Exception{
		
		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e os dados de classifica��o para o melhor algoritimo 
		calcularEstimativas(padrao, resultadoMLP1, resultadoMLP2, resultadoMLP3);
		
		// imprimi os resultados em tela
		imprimirResultadosBasicos();
	}

	private void calcularEstimativas(Padrao padrao, Resultado resultadoMLP1,
			Resultado resultadoMLP2, Resultado resultadoMLP3)
			throws Exception {

		
		double valorEstimado = 0;
		double valorEstimadoMLP1 = 0;
		double valorEstimadoMLP2 = 0;
		double valorEstimadoMLP3 = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());
		
		//obtem o modelo knn1 criado
		ObjectInputStream oisMLP1 = new ObjectInputStream(new FileInputStream(resultadoMLP1.getNomeModelo()));
		MultilayerPerceptron mlp1= (MultilayerPerceptron) oisMLP1.readObject();
		oisMLP1.close();

		//obtem o modelo knn3 criado
		ObjectInputStream oisMLP2 = new ObjectInputStream(new FileInputStream(resultadoMLP2.getNomeModelo()));
		MultilayerPerceptron mlp2 = (MultilayerPerceptron) oisMLP2.readObject();
		oisMLP2.close();
		
		//obtem o modelo knn5 criado
		ObjectInputStream oisMLP3 = new ObjectInputStream(new FileInputStream(resultadoMLP3.getNomeModelo()));
		MultilayerPerceptron mlp3 = (MultilayerPerceptron) oisMLP3.readObject();
		oisMLP3.close();
		
			
		//calcular a dist�ncia de cada elemento da lista de projetos valida��o para a lista de projetos avaliados
		for(int i = 0 ; i < instancias.numInstances() ; i++){
			
			//pega cada instancia do conjunto de valida��o com rotulo de esfor�o
			instancia = instancias.instance(i);
			
			// verifica o valor do classificador estimado

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor A
			valorEstimadoMLP1 = (double) (mlp1.classifyInstance(instancia));

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor B
			valorEstimadoMLP2 = (double) (mlp2.classifyInstance(instancia));

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor C
			valorEstimadoMLP3 = (double) (mlp3.classifyInstance(instancia));

			valorEstimado = (valorEstimadoMLP1 + valorEstimadoMLP2 + valorEstimadoMLP3) / 3;

			// recupera o valor real do esfor�o
			valorReal = (double) instancia.classValue();

			//calcula o erro absoluto
			erroAbsoluto = Math
					.abs(valorReal - (valorEstimado));
			
			//calcula o mre
			mre = erroAbsoluto / valorReal;
			
			//adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);
			
			//adiciona o mre a lista de mres
			resultado.getMres().add(mre*100);
		}
	}
	
	
	@Override
	public void run(Padrao padrao) {

		try {
			
			//treina o mlp1
			Base_MLP mlp1 = new Base_MLP();
			mlp1.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 1);

			//treina o mlp2
			Base_MLP mlp2 = new Base_MLP();
			mlp2.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 2);

			//treina o mlp3
			Base_MLP mlp3 = new Base_MLP();
			mlp3.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 3);
			
			
			//carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao, padrao.getTreinoTeste()));
					
			//avalia o modelo combinado
			avaliarMetodo(padrao, mlp1.getResultado(), mlp2.getResultado(), mlp3.getResultado());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public String toString() {
		return "MEANS_MLP";
	}


}
