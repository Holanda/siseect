package method;

import datas.Padrao;
import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.functions.LinearRegression;

public class Base_LR extends Tecnica {

	
	public static final String LR = "LR";
	LinearRegression lr;
	
	public Base_LR() {

	}

	public void criarModelo(String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);
		
		//usando valida��o cruzada para criar o modelo	
		WekaExperiment we = new WekaExperiment();		

		// por enquanto coloco diretamente as inst�ncias para treino e teste, quando selecionar os atributos faremos a filtragem antes
		we.setTrainingData(instancias);

		we.KFoldCrossValidationStratified(10, 1, WekaExperiment.LINEAR_REGRESSION); 
	
		// retorna o classificador gerado
		lr = (LinearRegression) we.getClassifier();

		gerarModelo(lr, LR + "-" + nomeArquivoTreino);

		
	}

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		
		lr = (LinearRegression) recuperarModelo();
		configurarDados(nomeArquivoTeste);

		resultado = new Resultado();
		resultado.avaliarModelo(lr, instancias, nomeModeloCriado);
	}

	@Override
	public void run(Padrao padrao) {
		try {

			Base_LR rl = new Base_LR();
			rl.criarModelo(padrao.getTreinoTeste());
			rl.usarModelo(padrao.getValidacao());

			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void run(String treino, String validacao) {
		try {

	
			criarModelo(treino);
			usarModelo(validacao);

			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	


}
