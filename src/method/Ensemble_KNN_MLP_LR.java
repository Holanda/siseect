package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import model.Resultado;
import model.ResultadoIdeal;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.Instance;
import datas.Padrao;

public class Ensemble_KNN_MLP_LR extends EnsembleTecnica {
	
	private static final String KNN = "KNN";
	private static final String MLP = "MLP";
	private static final String LR = "LR";

	public Ensemble_KNN_MLP_LR() {
	}
	
	private void avaliarMetodo(Padrao padrao, Resultado resultadoKNN, Resultado resultadoMLP,
			Resultado resultadoLR, Integer tipoClassificador ) throws Exception{
		
		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e os dados de classifica��o para o melhor algoritimo 
		calcularEstimativas(padrao, resultadoKNN, resultadoMLP, resultadoLR, tipoClassificador);
		
		// imprimi os resultados em tela
		imprimirResultados();
	}

	private void calcularEstimativas(Padrao padrao, Resultado resultadoKNN,
			Resultado resultadoMLP, Resultado resultadoLR, Integer tipoClassificador)
			throws Exception {

		
		double valorEstimado = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());
		
		//obtem o modelo knn criado
		ObjectInputStream oisKNN = new ObjectInputStream(new FileInputStream(resultadoKNN.getNomeModelo()));
		IBk knn = (IBk) oisKNN.readObject();
		oisKNN.close();

		//obtem o modelo mlp criado
		ObjectInputStream oisMLP = new ObjectInputStream(new FileInputStream(resultadoMLP.getNomeModelo()));
		MultilayerPerceptron mlp = (MultilayerPerceptron) oisMLP.readObject();
		oisMLP.close();
		
		//obtem o modelo knn5 criado
		ObjectInputStream oisLR = new ObjectInputStream(new FileInputStream(resultadoLR.getNomeModelo()));
		LinearRegression lr = (LinearRegression) oisLR.readObject();
		oisLR.close();
		
		// configura as instancias com o classificador de melhor tecnica e sem o classificador 		
		configurarDadosClassificacao(padrao.getTreinoClassificadorKNN_MLP_LR(), padrao.getValidacaoClassificador());
		
		// obtem o classificador treinado com as instancias que foram configuradas
		classificador = getClassificadorTreinado(tipoClassificador);

		// configura o r�tulo
		double rotuloClassificador = 0;
		Attribute rotulo = instanciasClassificadas.attribute(padrao.getIndiceRotuloClassificador());
		String algoritmoEscolhidoPeloClassificador;
		Instance instanciaSemClassificador;

		
		//calcular a dist�ncia de cada elemento da lista de projetos valida��o para a lista de projetos avaliados
		for(int i = 0 ; i < instancias.numInstances() ; i++){
			
			ResultadoIdeal resultadoIdeal;
			
			//pega cada instancia do conjunto de valida��o com rotulo de esfor�o
			instancia = instancias.instance(i);
			
			//pega cada instancia do conjunto de valida��o com rotulo de melhor algoritimo vazio
			instanciaSemClassificador = instanciasSemClassificacao.instance(i);
			
			//classifica a instancia com o melhor algoritimo usando algoritimo de classifica��o
			rotuloClassificador = classificador.classifyInstance(instanciaSemClassificador);
			
			//converte a classifica��o obtida para valor de caracteres que representa o melho algoritimo
			algoritmoEscolhidoPeloClassificador = rotulo.value((int)rotuloClassificador);

			//verifica o valor do classificador estimado
			if(algoritmoEscolhidoPeloClassificador.equals(KNN) ){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor knn
				valorEstimado = (double) (knn.classifyInstance(instancia));
				
			}else if(algoritmoEscolhidoPeloClassificador.equals(MLP)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor mlp
				valorEstimado = (double) (mlp.classifyInstance(instancia));

			}else if(algoritmoEscolhidoPeloClassificador.equals(LR)){

				// Classificamos esta inst�ncia com o algoritimo relativo ao valor lr
				valorEstimado = (double) (lr.classifyInstance(instancia));
				
			}else{
				throw new Exception("Melhor Algoritmo n�o encontrado");
			}

			// recupera o valor real do esfor�o
			valorReal = (double) instancia.classValue();
			
			// Processo para encontrar o melhor algoritimo na verdade e comparar com o escolhido			
			resultadoIdeal = obtemResultadoIdeal(valorReal,
					instancia, knn,  mlp, lr, algoritmoEscolhidoPeloClassificador);

			if (resultadoIdeal.isAcertouAlgoritmo()){
				resultado.addAcerto();
			}else{
				resultado.addErro();
			}

			//calcula o erro absoluto
			erroAbsoluto = Math
					.abs(valorReal - (valorEstimado));
			
			//calcula o mre
			mre = erroAbsoluto / valorReal;
			
			//adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);
			
			//adiciona o mre a lista de mres
			resultado.getMres().add(mre*100);
			
			//adiciona o mre ideal calculado no processo de encontrar o melhor algoritmo
			resultado.getMmresIdeais().add(resultadoIdeal.getMenorErroEstimado()*100);
		}
	}
	
	private ResultadoIdeal obtemResultadoIdeal(double valorReal,
			Instance instancia, IBk knn, MultilayerPerceptron mlp, LinearRegression lr,
			String algoritmoEscolhidoPeloClassificador) throws Exception {

		double valorEstimado, melhorValorEstimado;
		double erroAbs;
		double erroMre, menorErroMre;
		String melhorAlgoritmo;
		ResultadoIdeal resultadoIdeal = new ResultadoIdeal();

		
		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o knn
		valorEstimado = (double) (knn.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		
		melhorValorEstimado = valorEstimado;
		melhorAlgoritmo = KNN;
		menorErroMre = erroMre;
		

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o mlp1
		valorEstimado = (double) (mlp.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = MLP;
			menorErroMre = erroMre;
		}

		// Classificamos esta inst�ncia com o algoritimo relativo ao valor para o lr
		valorEstimado = (double) (lr.classifyInstance(instancia));
		erroAbs = Math.abs(valorReal - (valorEstimado));
		erroMre = erroAbs / valorReal;
		if (erroMre < menorErroMre){
			melhorValorEstimado = valorEstimado;
			melhorAlgoritmo = LR;
			menorErroMre = erroMre;
		}

		resultadoIdeal.setMelhorAlgoritmo(melhorAlgoritmo);
		resultadoIdeal.setMelhorValorEstimado(melhorValorEstimado);
		resultadoIdeal.setMenorErroEstimado(menorErroMre);
		
//		System.out.print("	" + resultadoIdeal.getMelhorAlgoritmo());
//		System.out.print("	" +  algoritmoEscolhidoPeloClassificador);
//		System.out.println();
		
		if(melhorAlgoritmo.equals(algoritmoEscolhidoPeloClassificador)){
			resultadoIdeal.setAcertouAlgoritmo(true);
		}else{
			resultadoIdeal.setAcertouAlgoritmo(false);
		}
		
		return resultadoIdeal;
	}
	
	
	@Override
	public void run(Padrao padrao, Integer tipoClassificador) {

		try {
			
			//treina o KNN
			Base_KNN knn = new Base_KNN();
			knn.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 3);

			//treina o MLP
			Base_MLP mlp = new Base_MLP();
			mlp.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 1);

			//treina o LR
			Base_LR lr = new Base_LR();
			lr.run(padrao.getTreinoTeste(), padrao.getTreinoTeste());
			
			
			//carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao, padrao.getTreinoTeste()));
			
			//adiciona os campos melhor algoritimo e menor erro a lista de projetos
			adicionarCamposListaProjetosAvaliados(knn.getResultado(), mlp.getResultado(), lr.getResultado());
			
			//avalia o modelo combinado
			//avaliarMetodo(padrao, knn.getResultado(), mlp.getResultado(), lr.getResultado(), tipoClassificador);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public String toString() {
		return "ESSEMBLE_KNN_MLP_LR";
	}
}
