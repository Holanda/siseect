package method;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import model.Resultado;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import datas.Padrao;

public class Means_KNN_MLP_LR extends MeansTecnica {

	public Means_KNN_MLP_LR() {
	}

	private void avaliarMetodo(Padrao padrao, Resultado resultadoKNN,
			Resultado resultadoMLP, Resultado resultadoLR) throws Exception {

		// carrega os dados de valida��o nas instancias principais
		configurarDados(padrao.getValidacao());

		// calcula as estimativas combinadas utilizando os resultados de KNN e
		// os dados de classifica��o para o melhor algoritimo
		calcularEstimativas(padrao, resultadoKNN, resultadoMLP, resultadoLR);

		// imprimi os resultados em tela
		imprimirResultadosBasicos();
	}

	private void calcularEstimativas(Padrao padrao, Resultado resultadoKNN,
			Resultado resultadoMLP, Resultado resultadoLR) throws Exception {

		double valorEstimado = 0;
		double valorEstimadoKNN = 0;
		double valorEstimadoMLP = 0;
		double valorEstimadoLR = 0;
		double valorReal = 0;
		double erroAbsoluto, mre;
		Instance instancia;
		resultado = new Resultado();
		resultado.setNomeModelo(toString() + " - " + padrao.toString());

		// obtem o modelo knn criado
		ObjectInputStream oisKNN = new ObjectInputStream(new FileInputStream(
				resultadoKNN.getNomeModelo()));
		IBk knn = (IBk) oisKNN.readObject();
		oisKNN.close();

		// obtem o modelo mlp criado
		ObjectInputStream oisMLP = new ObjectInputStream(new FileInputStream(
				resultadoMLP.getNomeModelo()));
		MultilayerPerceptron mlp = (MultilayerPerceptron) oisMLP.readObject();
		oisMLP.close();

		// obtem o modelo knn5 criado
		ObjectInputStream oisLR = new ObjectInputStream(new FileInputStream(
				resultadoLR.getNomeModelo()));
		LinearRegression lr = (LinearRegression) oisLR.readObject();
		oisLR.close();

		// calcular a dist�ncia de cada elemento da lista de projetos valida��o
		// para a lista de projetos avaliados
		for (int i = 0; i < instancias.numInstances(); i++) {

			// pega cada instancia do conjunto de valida��o com rotulo de
			// esfor�o
			instancia = instancias.instance(i);

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor
			// knn
			valorEstimadoKNN = (double) (knn.classifyInstance(instancia));

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor
			// mlp
			valorEstimadoMLP = (double) (mlp.classifyInstance(instancia));

			// Classificamos esta inst�ncia com o algoritimo relativo ao valor
			// lr
			valorEstimadoLR = (double) (lr.classifyInstance(instancia));

			valorEstimado = (valorEstimadoKNN + valorEstimadoMLP + valorEstimadoLR) / 3;

			// recupera o valor real do esfor�o
			valorReal = (double) instancia.classValue();

			// calcula o erro absoluto
			erroAbsoluto = Math.abs(valorReal - (valorEstimado));

			// calcula o mre
			mre = erroAbsoluto / valorReal;

			// adiciona o erro a lista de erros absolutos
			resultado.getErrosAbsolutos().add(erroAbsoluto);

			// adiciona o mre a lista de mres
			resultado.getMres().add(mre * 100);

		}
	}

	@Override
	public void run(Padrao padrao) {

		try {

			// treina o KNN
			Base_KNN knn = new Base_KNN();
			knn.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 3);

			// treina o MLP
			Base_MLP mlp = new Base_MLP();
			mlp.run(padrao.getTreinoTeste(), padrao.getTreinoTeste(), 1);

			// treina o LR
			Base_LR lr = new Base_LR();
			lr.run(padrao.getTreinoTeste(), padrao.getTreinoTeste());

			// carrega a lista de projetos que foram testados
			setListaProjetos(Padrao.converteInstanciasParaProjetos(padrao,
					padrao.getTreinoTeste()));

			avaliarMetodo(padrao, knn.getResultado(), mlp.getResultado(),
					lr.getResultado());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "MEANS_KNN_MLP_LR";
	}
}
