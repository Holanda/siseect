package method;

import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.meta.AdditiveRegression;
import datas.Padrao;

public class Base_AR extends Tecnica {

	
	public static final String ADDITIVE_REGRESSION = "ADDITIVE_REGESSION";
	private AdditiveRegression additiveRegression;

	public Base_AR() {

	}

	public void criarModelo(String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);

		we = new WekaExperiment();

		we.setTrainingData(instancias);

		we.KFoldCrossValidationStratified(10, 1, WekaExperiment.ADDITIVE_REGRESSION); 
		
		
		// retorna o classificador gerado
		additiveRegression = (AdditiveRegression) we.getClassifier();

		gerarModelo(additiveRegression, ADDITIVE_REGRESSION + "-" + nomeArquivoTreino);
		
	}

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		additiveRegression = (AdditiveRegression) recuperarModelo();
		configurarDados(nomeArquivoTeste);
		
		resultado = new Resultado();
		resultado.avaliarModelo(additiveRegression, instancias, nomeModeloCriado);
	}

	public void run(String treino, String validacao) {
		try {
			
			criarModelo(treino);
			usarModelo(validacao);
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(Padrao padrao) {
		
		try {

			Base_AR regressaoAditiva = new Base_AR();
			regressaoAditiva.criarModelo(padrao.getTreino());
			regressaoAditiva.usarModelo(padrao.getValidacao());
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}


}
