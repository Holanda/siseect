package method;

import model.Resultado;
import util.WekaExperiment;
import weka.classifiers.lazy.KStar;
import datas.Padrao;

public class Base_KS extends Tecnica {

	
	public static final String KSTAR = "KSTAR";
	private KStar kStar;

	public Base_KS() {

	}

	public void criarModelo(String nomeArquivoTreino) throws Exception {

		configurarDados(nomeArquivoTreino);

		we = new WekaExperiment();

		we.setTrainingData(instancias);

		we.KFoldCrossValidationStratified(10, 1, WekaExperiment.KSTAR); 
		
		
		// retorna o classificador gerado
		kStar = (weka.classifiers.lazy.KStar) we.getClassifier();

		gerarModelo(kStar, KSTAR + "-" + nomeArquivoTreino);
		
	}

	public void usarModelo(String nomeArquivoTeste) throws Exception {

		kStar = (KStar) recuperarModelo();
		configurarDados(nomeArquivoTeste);
		
		resultado = new Resultado();
		resultado.avaliarModelo(kStar, instancias, nomeModeloCriado);
	}

	public void run(String treino, String validacao) {
		try {
			
			criarModelo(treino);
			usarModelo(validacao);
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(Padrao padrao) {
		
		try {

			Base_KS kStar = new Base_KS();
			kStar.criarModelo(padrao.getTreino());
			kStar.usarModelo(padrao.getValidacao());
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}


}
