package main;
import method.Base_KNN;
import datas.PadraoA1;
import datas.PadraoA2;
import datas.PadraoB1;

public class TesteKNN {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("----- Teste KNN-IBk utilizando padr�o A1 -----");
		new Base_KNN().run(new PadraoA1());

		System.out.println();
		System.out.println("----- Teste KNN-IBk utilizando padr�o A2 -----");
		//new Base_KNN().run(new PadraoA2());
		
		System.out.println();
		System.out.println("----- Teste KNN-IBk utilizando padr�o A3 -----");
		//new Base_KNN().run(new PadraoA3());

		System.out.println();
		System.out.println("----- Teste KNN-IBk utilizando padr�o B1 -----");
		//new Base_KNN().run(new PadraoB1());
		
	}

}
