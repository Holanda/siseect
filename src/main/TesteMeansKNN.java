package main;
import method.Means_KNN;
import datas.PadraoA1;
import datas.PadraoA2;
import datas.PadraoB1;

public class TesteMeansKNN {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("----- Teste KNN-IBk M�dia utilizando padr�o A1 -----");
		new Means_KNN().run(new PadraoA1());

		System.out.println("----- Teste KNN-IBk M�dia utilizando padr�o A2 -----");
		new Means_KNN().run(new PadraoA2());

		System.out.println("----- Teste KNN-IBk M�dia utilizando padr�o B1 -----");
		new Means_KNN().run(new PadraoB1());

		
	}

}
