package main;
import util.WekaExperiment;
import datas.PadraoA1;
import datas.PadraoA2;
import method.*;

public class TesteEnsemble_KNN3_MLP2_KSTAR_MLP1_LR_AR_GP {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		try {
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando Naive Bayes e o Padr�o A1");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA1(), WekaExperiment.NAIVE_BAYES);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando J48 e o Padr�o A1");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA1(), WekaExperiment.J48);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando Regress�o Logistica e o Padr�o A1");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA1(), WekaExperiment.LOGISTIC_REGRESSION);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando SVM e o Padr�o A1");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA1(), WekaExperiment.SVM);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando Adaboost e o Padr�o A1");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA1(), WekaExperiment.ADABOOST);


			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando Naive Bayes e o Padr�o A2");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA2(), WekaExperiment.NAIVE_BAYES);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando J48 e o Padr�o A2");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA2(), WekaExperiment.J48);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando Regress�o Logistica e o Padr�o A2");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA2(), WekaExperiment.LOGISTIC_REGRESSION);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando SVM e o Padr�o A2");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA2(), WekaExperiment.SVM);
			System.out.println("----- Teste com Essemble Ensemble_KNN3_KNN5_KS_MLP_LR_AR_GP utilizando Adaboost e o Padr�o A2");
			new Ensemble_KNN3_MLP2_KS_MLP1_LR_AR_GP().run(new PadraoA2(), WekaExperiment.ADABOOST);

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

}
