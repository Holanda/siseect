package main;
import method.Ensemble_KNN_MLP_LR;
import util.WekaExperiment;
import datas.PadraoA1;
import datas.PadraoA2;
import datas.PadraoB1;

public class TesteEnsembleKNN_MLP_LR {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		try {

		
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Naive Bayes e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.NAIVE_BAYES);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando J48 e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.J48);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Regress�o Logistica e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.LOGISTIC_REGRESSION);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando SVM e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.SVM);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Adaboost e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.ADABOOST);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando KNN5 e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.KNN5);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando KNN3 e o Padr�o A1");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA1(), WekaExperiment.KNN3);

//			System.out.println();
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Naive Bayes e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.NAIVE_BAYES);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando J48 e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.J48);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Regress�o Logistica e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.LOGISTIC_REGRESSION);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando SVM e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.SVM);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Adaboost e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.ADABOOST);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando KNN5 e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.KNN5);
//			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando KNN3 e o Padr�o A2");
//			new Ensemble_KNN_MLP_LR().run(new PadraoA2(), WekaExperiment.KNN3);

			System.out.println();
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Naive Bayes e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.NAIVE_BAYES);
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando J48 e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.J48);
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Regress�o Logistica e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.LOGISTIC_REGRESSION);
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando SVM e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.SVM);
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando Adaboost e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.ADABOOST);
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando KNN5 e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.KNN5);
			System.out.println("----- Teste com Essemble KNN_MLP_LR utilizando KNN3 e o Padr�o B1");
			new Ensemble_KNN_MLP_LR().run(new PadraoB1(), WekaExperiment.KNN3);
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

}
