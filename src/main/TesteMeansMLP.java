package main;
import method.Means_MLP;
import datas.PadraoA1;
import datas.PadraoA2;
import datas.PadraoB1;

public class TesteMeansMLP {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("----- Teste MLP M�dia utilizando padr�o A1 -----");
		new Means_MLP().run(new PadraoA1());

		System.out.println("----- Teste MLP M�dia utilizando padr�o A2 -----");
		new Means_MLP().run(new PadraoA2());

		System.out.println("----- Teste MLP M�dia utilizando padr�o B1 -----");
		new Means_MLP().run(new PadraoB1());

		
	}

}
