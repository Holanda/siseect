package main;
import method.Means_KNN_MLP_LR;
import datas.PadraoA1;
import datas.PadraoA2;
import datas.PadraoB1;

public class TesteMeansKNN_MLP_LR {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("----- Teste KNN_MLP_LR M�dia utilizando padr�o A1 -----");
		new Means_KNN_MLP_LR().run(new PadraoA1());

		System.out.println("----- Teste KNN_MLP_LR M�dia utilizando padr�o A2 -----");
		new Means_KNN_MLP_LR().run(new PadraoA2());

		System.out.println("----- Teste KNN_MLP_LR M�dia utilizando padr�o B1 -----");
		new Means_KNN_MLP_LR().run(new PadraoB1());

		
	}

}
