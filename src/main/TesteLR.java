package main;

import method.Base_LR;
import datas.PadraoA1;
import datas.PadraoA2;
import datas.PadraoA3;
import datas.PadraoB1;

public class TesteLR {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("----- Teste com Linear Regression utilizando o padr�o A1 -----");
		new Base_LR().run(new PadraoA1());
		

		System.out.println();
		System.out.println("----- Teste com Linear Regression utilizando o padr�o A2 -----");
		new Base_LR().run(new PadraoA2());

		System.out.println();
		System.out.println("----- Teste com Linear Regression utilizando o padr�o B1 -----");
		//new Base_LR().run(new PadraoA3());

		System.out.println();
		System.out.println("----- Teste com Linear Regression utilizando o padr�o B1 -----");
		new Base_LR().run(new PadraoB1());
	}

}
