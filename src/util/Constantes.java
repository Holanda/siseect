package util;

/**
 * 
 * Classe que define o caminho do conjunto de dados que ser�o testados na
 * aplica��o
 * 
 * */

public class Constantes {
	/**
	 * Caminho do conjunto de dados 01 - Dados - Vazios Preenchidos e
	 * Transformados com CS e Web completos sem arquitetura vazio e atributos
	 * desnecess�rios
	 */
	//public static final String CAMINHO_PADRAO = "F:/Dropbox/Documentos/Doutorado/Tese/Dados/R�tulo Produtividade/Atributos entrada discretos/Dados ajustados/Dados preparados para java/";
	public static final String CAMINHO_PADRAO = "F:/Dropbox/Documentos/Doutorado/Tese/Dados ISBSG/";
	public static final boolean IMPRIMIR_MELHOR_ALGORITMO = false;
	public static final boolean IMPRIMIR_RESULTADO_INDIVIDUAL = true;
	public static final boolean IMPRIMIR_ERRO_EXEMPLO = true;




}
